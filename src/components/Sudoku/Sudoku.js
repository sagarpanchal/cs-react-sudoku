import React, { useState } from 'react'
import { Dropdown, DropdownItem } from 'reactstrap'
import { DropdownToggle } from 'reactstrap'
import { DropdownMenu } from 'reactstrap'

import { SudokuInterface } from 'libraries/Sudoku'
import classNames from 'utils/classNames'
import styles from './Sudoku.module.scss'

let sudoku = new SudokuInterface()

export default function Sudoku() {
  const [puzzle, setPuzzle] = useState([...sudoku.puzzle])
  const [showHints, setShowHint] = useState(false)
  const [newDropdown, setnewDropdown] = useState(false)

  const newPuzzle = (size = 9) => {
    sudoku = new SudokuInterface({ size })
    setPuzzle([...sudoku.puzzle])
  }
  const resetPuzzle = () => setPuzzle([...sudoku.puzzle])
  const solvePuzzle = () => setPuzzle([...sudoku.solution])
  const toggleHints = () => setShowHint((prevState) => !prevState)
  const toggleNewDropdown = () => setnewDropdown((prevState) => !prevState)

  const setElement = (index, val) => {
    const value = parseInt(val) || ''
    if (puzzle[index] === value) return

    const newPuzzle = [...puzzle]
    newPuzzle[index] = parseInt(value) || ''
    setPuzzle(newPuzzle)
  }

  const focusInput = (index, steps) => {
    if (index < 0 || index >= sudoku.length) return
    const element = document.querySelector(`#input-${index}`)
    if (!element?.disabled) {
      element.setSelectionRange(0, element.value.length)
      element.focus()
      return
    }
    focusInput(index + steps, steps)
  }

  const handleKeyDown = (e, index) => {
    if (![37, 39, 38, 40].includes(e.keyCode)) return
    e.preventDefault()
    const keymap = { '37': -1, '39': +1, '38': -sudoku.size, '40': +sudoku.size }
    const steps = keymap[`${e.keyCode}`]
    focusInput(index + steps, steps)
  }

  const matchesMap = puzzle.map((el, i) => el === sudoku.solution[i])
  const remainingBoxes = matchesMap.filter((m) => !m).length
  const isFinished = remainingBoxes === 0

  return (
    <div className={styles.Sudoku}>
      <div className={classNames([styles.board, sudoku.size === 6 && styles.x6, sudoku.size === 9 && styles.x9, 'cf'])}>
        {Array(sudoku.length)
          .fill(undefined)
          .map((u, i) => (
            <input
              type="text"
              key={i}
              id={`input-${i}`}
              value={`${puzzle[i]}`}
              maxLength={1}
              onChange={({ target: { value } }) => setElement(i, value)}
              className={classNames([
                sudoku.puzzle[i] !== ''
                  ? sudoku.puzzle[i] === puzzle[i] && 'text-dark bg-gray'
                  : (!matchesMap.includes(false) || showHints) &&
                    (matchesMap[i] ? styles.valid : puzzle[i] !== '' && styles.invalid),
              ])}
              disabled={
                (puzzle[i] !== '' && sudoku.puzzle[i] === puzzle[i]) ||
                ((!matchesMap.includes(false) || showHints) && matchesMap[i])
              }
              onKeyDown={(e) => handleKeyDown(e, i)}
            />
          ))}
        <div className={classNames(['w-100', 'mt-3', 'text-center'])}>
          {isFinished ? (
            <span>Congratulations! You have finished the puzzle!</span>
          ) : (
            <span>{`${remainingBoxes}/${matchesMap.length} Remaining`}</span>
          )}
        </div>
        <div className={classNames(['w-100', 'mt-3', 'text-center'])}>
          {!isFinished && (
            <>
              <button className="btn btn-sm btn-primary " onClick={toggleHints}>
                {showHints ? 'Disable' : 'Enable'} Hints
              </button>
              <span className="mx-1"></span>
              <button className="btn btn-sm btn-primary " onClick={solvePuzzle}>
                Solve
              </button>
            </>
          )}
          <span className="mx-1"></span>
          <button className="btn btn-sm btn-primary" onClick={() => resetPuzzle()}>
            Reset
          </button>
          <span className="mx-1"></span>
          <Dropdown isOpen={newDropdown} toggle={toggleNewDropdown} className="d-inline-block">
            <DropdownToggle caret size="sm" color="primary">
              New
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem onClick={() => newPuzzle(9)}>Regular 9×9</DropdownItem>
              <DropdownItem onClick={() => newPuzzle(6)}>Small 6×6</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    </div>
  )
}
