import React, { Component } from 'react'
import Sudoku from 'components/Sudoku/Sudoku'
import 'sass/_app.scss'

export default class App extends Component {
  render() {
    return (
      <>
        <Sudoku />
      </>
    )
  }
}
