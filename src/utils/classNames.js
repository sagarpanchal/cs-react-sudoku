export default (classArray) => classArray.filter(Boolean).join(' ').trim()
