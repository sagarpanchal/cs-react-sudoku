class Sudoku {
  constructor(props = {}) {
    props = { size: 9, ...props }
    this.__rating__ = 0
    this.__solvedPuzzle__ = []
    this.size = props.size
    this.length = props.size * props.size
    this.difficulty = 0
    this.puzzle = []
    this._make()
  }

  _make() {
    const board = this.solve(Array(this.length).fill(undefined))
    const puzzle = []
    const deduced = Array(this.length).fill(undefined)
    const order = [...Array(this.length).keys()]
    this._shuffleArray(order)

    // console.log({ board, order, deduced});
    order.forEach((pos) => {
      if (deduced[pos] !== undefined) return
      puzzle.push({ pos, num: board[pos] })
      deduced[pos] = board[pos]
      this._deduce(deduced)
    })

    // console.log({ deduced, puzzle });
    this._shuffleArray(puzzle)

    for (let i = puzzle.length - 1; i >= 0; i--) {
      let e = puzzle[i]
      this._removeElement(puzzle, i)
      let rating = this._check(this._boardForEntries(puzzle), board)
      if (rating === -1) puzzle.push(e)
    }

    this.puzzle = this._boardForEntries(puzzle)
    this.rate(this.puzzle, 1)
  }

  rate(puzzle = this.puzzle, samples = 10) {
    return new Promise((resolve) => {
      let total = 0

      for (let i = 0; i < samples; i++) {
        const tuple = this._solveBoard(puzzle)

        if (tuple.answer === undefined) {
          this.__rating__ = -1
          return resolve(this.__rating__)
        }

        total += tuple.state.length
      }

      this.__rating__ = total / samples

      return resolve(this.__rating__)
    })
  }

  _check(puzzle = this.puzzle, board) {
    if (board === undefined) board = undefined

    const tuple1 = this._solveBoard(puzzle)

    if (tuple1.answer === undefined) return -1

    if (board !== undefined && !this._boardMatches(board, tuple1.answer)) return -1

    const difficulty = tuple1.state.length
    const tuple2 = this._solveNext(tuple1.state)

    this.difficulty = tuple2.answer !== undefined ? -1 : difficulty
    return this.difficulty
  }

  solve(board = this.puzzle) {
    this.__solvedPuzzle__ = this._solveBoard(board).answer
    return [...this.__solvedPuzzle__]
  }

  _solveBoard(original) {
    const board = [].concat(original)
    const guesses = this._deduce(board)

    if (guesses === undefined) return { state: [], answer: board }

    const track = [{ guesses: guesses, count: 0, board: board }]

    return this._solveNext(track)
  }

  _solveNext(remembered) {
    while (remembered.length > 0) {
      let tuple1 = remembered.pop()

      if (tuple1.count >= tuple1.guesses.length) {
        continue
      }

      remembered.push({
        guesses: tuple1.guesses,
        count: tuple1.count + 1,
        board: tuple1.board,
      })

      let workspace = [].concat(tuple1.board)
      let tuple2 = tuple1.guesses[tuple1.count]

      workspace[tuple2.pos] = tuple2.num

      let guesses = this._deduce(workspace)
      if (guesses === undefined) return { state: remembered, answer: workspace }

      remembered.push({ guesses: guesses, count: 0, board: workspace })
    }

    return { state: [], answer: undefined }
  }

  _deduce(board) {
    while (true) {
      let stuck = true
      let guess = undefined
      let count = 0 // fill in any spots determined by direct conflicts

      const tuple1 = this._figureBits(board)
      let allowed = tuple1.allowed
      let needed = tuple1.needed

      for (let pos = 0; pos < this.length; pos++) {
        if (board[pos] === undefined) {
          const numbers = this._listBits(allowed[pos])

          if (numbers.length === 0) {
            return []
          } else if (numbers.length === 1) {
            board[pos] = numbers[0]
            stuck = false
          } else if (stuck) {
            const t = numbers.map((num) => ({ pos, num }))
            const tuple2 = this._pickBetter(guess, count, t)
            guess = tuple2.guess
            count = tuple2.count
          }
        }
      }

      if (!stuck) {
        const tuple3 = this._figureBits(board)
        allowed = tuple3.allowed
        needed = tuple3.needed
      } // fill in any spots determined by elimination of other locations

      for (let axis = 0; axis < this.size / 3; axis++) {
        for (let x = 0; x < this.size; x++) {
          const numbers = this._listBits(needed[axis * this.size + x])

          for (let i = 0; i < numbers.length; i++) {
            const num = numbers[i]
            const bit = 1 << num
            const spots = []

            for (let y = 0; y < this.size; y++) {
              const pos = this.positionFor(x, y, axis)
              if (allowed[pos] & bit) spots.push(pos)
            }

            if (spots.length === 0) {
              return []
            } else if (spots.length === 1) {
              board[spots[0]] = num
              stuck = false
            } else if (stuck) {
              const t = spots.map((pos) => ({ pos, num }))
              const tuple4 = this._pickBetter(guess, count, t)
              guess = tuple4.guess
              count = tuple4.count
            }
          }
        }
      }

      if (stuck) {
        if (guess !== undefined) this._shuffleArray(guess)
        return guess
      }
    }
  }

  _figureBits(board) {
    const needed = []
    const allowed = board.map((val) => (val === undefined ? 511 : 0), [])

    for (let axis = 0; axis < this.size / 3; axis++) {
      for (let x = 0; x < this.size; x++) {
        const bits = this._axisMissing(board, x, axis)
        needed.push(bits)

        for (let y = 0; y < this.size; y++) {
          const pos = this.positionFor(x, y, axis)
          allowed[pos] = allowed[pos] & bits
        }
      }
    }

    return { allowed, needed }
  }

  positionFor(x, y, axis) {
    if (axis === undefined) axis = 0

    return axis === 0
      ? x * this.size + y
      : axis === 1
      ? y * this.size + x
      : [0, 3, 6, 27, 30, 33, 54, 57, 60][x] + [0, 1, 2, 9, 10, 11, 18, 19, 20][y]
  }

  _axisMissing(board, x, axis) {
    let bits = 0

    for (let y = 0; y < this.size; y++) {
      const element = board[this.positionFor(x, y, axis)]

      if (element !== undefined) bits |= 1 << element
    }

    return 511 ^ bits
  }

  _listBits(bits) {
    const list = []

    for (let y = 0; y < this.size; y++) {
      if ((bits & (1 << y)) !== 0) {
        list.push(y)
      }
    }

    return list
  }

  _pickBetter(b, c, t) {
    return b === undefined || t.length < b.length
      ? { guess: t, count: 1 }
      : t.length > b.length
      ? { guess: b, count: c }
      : this._randomInt(c) === 0
      ? { guess: t, count: c + 1 }
      : { guess: b, count: c + 1 }
  }

  _boardForEntries(entries) {
    const board = Array(this.length).fill(undefined)

    entries.forEach((item) => {
      const pos = item.pos
      const num = item.num
      board[pos] = num
    })

    return board
  }

  _boardMatches(b1, b2) {
    for (let i = 0; i < this.length; i++) {
      if (b1[i] !== b2[i]) return false
    }

    return true
  }

  _randomInt(max) {
    return Math.floor(Math.random() * (max + 1))
  }

  _shuffleArray(original) {
    // Swap each element with another randomly selected one.
    for (let i = 0; i < original.length; i++) {
      let j = i

      while (j === i) {
        j = Math.floor(Math.random() * original.length)
      }

      const contents = original[i]
      original[i] = original[j]
      original[j] = contents
    }
  }

  _removeElement(array, from, to) {
    const rest = array.slice((to || from) + 1 || array.length)
    array.length = from < 0 ? array.length + from : from
    return array.push.apply(array, rest)
  }
}

export class SudokuInterface {
  constructor(options = {}) {
    const sudoku = new Sudoku()
    this.size = sudoku.size
    this.length = sudoku.length
    this.puzzle = sudoku.puzzle.map((i) => (i === undefined ? '' : i + 1))
    this.solution = sudoku.__solvedPuzzle__.map((i) => i + 1)

    if (options?.size < 9) {
      this.size = options.size
      this.length = this.size * this.size

      let total = 0
      const stripBoard = (e, i) => {
        if (i === 0) total = 0
        const allow = i % 9 < this.size
        if (allow) total += 1
        if (total > this.length) return false
        return allow
      }

      this.puzzle = this.puzzle.filter(stripBoard)
      this.solution = this.solution.filter(stripBoard)
    }
  }
}
