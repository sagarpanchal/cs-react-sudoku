;# Setup Instructions

### Clone this repo in current directory

**Create and switch to a new empty directory and run following command**

```
git clone https://gitlab.com/sagarpanchal/cs-react-sudoku.git .
```

**Install all dependencies**

```
npm i   # install required npm packages
```

**Run the project**

```
npm start   # for live updates,
npm build   # for development build.
```
